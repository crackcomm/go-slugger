// Package slugger implements slug generator.
//
// Slugs are stored in store and are generated uniquely for ID.
package slugger

import (
	"fmt"
	"strings"
	"sync"

	"github.com/gosimple/slug"

	"bitbucket.org/crackcomm/go-store/pkg/store"
)

// Slugger - Slug generator.
type Slugger struct {
	Lang string

	store store.Store
	mutex *sync.RWMutex
}

// NewSlugger - Creates new slug generator.
func NewSlugger(s store.Store, lang string) *Slugger {
	// If store was not set use DefaultStore
	if s == nil {
		s = store.DefaultStore
	}

	return &Slugger{
		Lang:  lang,
		store: s,
		mutex: new(sync.RWMutex),
	}
}

// Simple - Just slugifies text.
func (s *Slugger) Simple(text string) string {
	return slug.MakeLang(text, s.Lang)
}

// Get - Gets slug ID.
func (s *Slugger) Get(namespace, slug string) (string, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.get(namespace, slug)
}

// New - Creates a new slug from string.
func (s *Slugger) New(namespace, id, text string) (result string, err error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// Create slug from text
	// And iterate until we have a free slug
	result, err = s.iterate(namespace, id, s.Simple(text), 0)
	return
}

// Exists - Checks if slug exists in namespace.
func (s *Slugger) Exists(namespace, slug string) (bool, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.exists(namespace, slug)
}

// Is - Checks if slug in namespace belongs to ID.
func (s *Slugger) Is(namespace, id, slug string) (bool, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.is(namespace, id, slug)
}

// exists - Checks if slug exists in namespace. (without locking)
func (s *Slugger) exists(namespace, slug string) (yes bool, err error) {
	// Get slug key
	key := s.key(namespace, slug)

	// Check if slug exists in store
	value, err := s.store.Get(key)
	if err != nil {
		return
	}

	// If value is not nil slug already exists
	yes = (value == nil)
	return
}

// is - Checks if slug in namespace belongs to ID. (without locking)
func (s *Slugger) is(namespace, id, slug string) (yes bool, err error) {
	// Get slug key
	key := s.key(namespace, slug)

	// Check if slug exists in store
	value, err := s.store.GetString(key)
	if err != nil {
		return
	}

	// Check if value is ID
	yes = (value == id)

	return
}

// iterate - Iterates a slug until finds a free slug.
func (s *Slugger) iterate(ns, id, slug string, n int) (txt string, err error) {
	// If n is not zero we have to add a digit to a slug
	if n == 0 {
		txt = slug
	} else {
		txt = fmt.Sprintf("%s-%d", slug, n)
	}

	// Get ID of a slug from store
	has, err := s.get(ns, txt)
	if err == store.ErrNotFound {
		// Save slug in store and return
		err = s.set(ns, id, txt)
		return
	} else if err != nil {
		return
	}

	// If slug ID is current id then return this slug
	if has == id {
		return
	}

	// If slug was found but belongs to other ID go with another digit
	return s.iterate(ns, id, slug, n+1)
}

// get - Gets slug ID. (without locking)
func (s *Slugger) get(namespace, slug string) (id string, err error) {
	// Get slug key
	key := s.key(namespace, slug)
	// Get id from store
	return s.store.GetString(key)
}

// set - Saves slug for ID in store.
func (s *Slugger) set(namespace, id, slug string) error {
	key := s.key(namespace, slug)
	return s.store.SetString(key, id)
}

// key - Creates namespace slug key.
func (s *Slugger) key(namespace, slug string) string {
	return strings.Join([]string{"slug", namespace, slug}, ":")
}
