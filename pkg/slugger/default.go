package slugger

import (
	"flag"

	"bitbucket.org/crackcomm/go-store/pkg/store"
)

// DefaultSlugger - Default Slugger.
var DefaultSlugger = NewSlugger(nil, "en")

// InitFlags - Initializes flags in default FlagSet.
func InitFlags() {
	flag.StringVar(&DefaultSlugger.Lang, "slug-lang", DefaultSlugger.Lang, "Slug language")
}

// Init - Initializes a default slugger using store.DefaultStore.
func Init() {
	if !flag.Parsed() {
		flag.Parse()
	}

	// Initialize default store if need to
	if store.DefaultStore == nil {
		store.Init()
	}

	// Set slugger default store
	DefaultSlugger.store = store.DefaultStore
}

// Simple - Just slugifies text.
func Simple(text string) string {
	return DefaultSlugger.Simple(text)
}

// Get - Gets slug ID using DefaultSlugger.
func Get(namespace, slug string) (string, error) {
	return DefaultSlugger.Get(namespace, slug)
}

// New - Creates a new slug from string using DefaultSlugger.
func New(namespace, id, text string) (string, error) {
	return DefaultSlugger.New(namespace, id, text)
}

// Exists - Checks if slug exists in namespace.
func Exists(namespace, slug string) (bool, error) {
	return DefaultSlugger.Exists(namespace, slug)
}

// Is - Checks if slug in namespace belongs to ID.
func Is(namespace, id, slug string) (bool, error) {
	return DefaultSlugger.Is(namespace, id, slug)
}
