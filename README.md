# go-slugger

Package slugger implements slug generator.
Slugs are stored in [store](https://bitbucket.org/crackcomm/go-store/) and are generated uniquely for ID.
